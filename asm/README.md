form terminal go to asm folder and run :

`go run asm.go -out ../sum.s -stubs ../stub.go`

will create 2 file on folder coba_avo (parent of asm folder) ,

- sum.s :

```
// Code generated by command: go run asm.go -out ../sum.s -stubs ../stub.go. DO NOT EDIT.

#include "textflag.h"

// func Sum(xs []byte) uint64
TEXT ·Sum(SB), NOSPLIT, $0-32
	MOVQ xs_base+0(FP), AX
	MOVQ xs_len+8(FP), CX

	// Initialize sum register to zero.
	XORQ DX, DX

loop:
	// Loop until zero bytes remain.
	CMPQ CX, $0x00
	JE   done

	// Load from pointer and add to running sum.
	MOVQ (AX), BX
	ANDQ $0xff, BX
	ADDQ BX, DX

	// Advance pointer, decrement byte count.
	INCQ AX
	DECQ CX
	JMP  loop

done:
	// Store sum to return value.
	MOVQ DX, ret+24(FP)
	RET
```

- and stub.go :

```
// Code generated by command: go run asm.go -out ../sum.s -stubs ../stub.go. DO NOT EDIT.

package asm

// Sum returns the sum of the elements in xs.
func Sum(xs []byte) uint64
```

then go to folder coba_avo and run :

```
go mod init
go mod tidy
```

then go to main folder and run :

`go run main.go`
